#!/usr/bin/env python3

'''
Created on 11 Apr 2019

@author: wonko
'''

from declarative_resume.pdf import Document
from declarative_resume.stylesheet_soffice import soffice
from declarative_resume.stylesheet_solarized import solarized


if __name__ == '__main__':

    sections = [
        ['title', 'title'],
        ['abstract', 'abstract'],
        ['stages', 'life'],
        ['framebreak'],
        ['contact', 'sections'],

        ['languages', 'sections'],
        ['methodologies', 'sections'],
        ['certifications', 'sections'],
        ['expertise', 'sections'],

        ['pagebreak', 'lists'],
        ['interest', 'lists'],
        ['framebreak'],
        ['projects', 'projects']
    ]

    doc = Document("john_watson.json", sections)

    doc.render("john_watson-en.pdf", soffice, 'en'),
    doc.render("john_watson-de.pdf", soffice, 'de'),
    doc.render("john_watson-solarized-de.pdf", solarized, 'de'),
    doc.render("john_watson-solarized-en.pdf", solarized, 'en'),
