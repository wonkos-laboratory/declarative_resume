'''
Created on 11 Apr 2019

@author: wonko
'''
from fractions import Fraction
import logging

from reportlab.lib.colors import black
from reportlab.lib.styles import ParagraphStyle as PlatypusParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus.flowables import Flowable
from reportlab.platypus.paragraph import Paragraph
from reportlab.platypus.tables import TableStyle, Table
from svglib.svglib import svg2rlg


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger('style_paragraph')
LOG.setLevel(logging.DEBUG)


class ParagraphStyle(PlatypusParagraphStyle):

    def __init__(self, name, parent=None, **kw):
        # todo file patch upstream that replaces the class compare with isinstance
        # step two
        self.name = name
        self.parent = parent
        self.__dict__.update(self.defaults)

        # step two - copy from parent if any.  Try to be
        # very strict that only keys in class defaults are
        # allowed, so they cannot inherit
        self.refresh()
        self._setKwds(**kw)


class InlineHeadingParagraphStyle(ParagraphStyle):
    pass


class UnderlinedParagraphStyle(ParagraphStyle):
    pass


class ListTableStyle(ParagraphStyle, TableStyle):

    def __init__(self, name, parent=None, **kw):

        # cmd=kw.get('cmd',[])
        ParagraphStyle.__init__(self, name, parent, **kw)
        TableStyle.__init__(self, **kw)


class listTable(Flowable):

    def __init__(self, data, stylesheet, style, level):
        self.data = data
        sty = stylesheet.styles.getstyle(style, 'tab', level)
        paraP.log.debug(style + "-TAB" + str(level) + "(" + str(sty) + "): ")
        paraP.log.debug(str(sty.parent))
        if isinstance(sty, TableStyle):
            self.style = sty
        else:
            self.style = None
        Flowable.__init__(self)

    def wrap(self, availWidth, availHeight):
        lmax = 0
        for r in self.data:
            if len(r) > 0:
                lmax = max(lmax, r[0].minWidth())
        colWidths = [lmax, availWidth - lmax]
        self.table = Table(self.data, colWidths)
        self.table.setStyle(self.style)
        return self.table.wrap(availWidth, availHeight)

    def draw(self):
        self.table._drawOn(self.canv)


class BargaugeStyle(ParagraphStyle):

    defaults = ParagraphStyle.defaults
    defaults['segments'] = 10
    defaults['borderColor'] = black


class bargauge(Flowable):
    log = LOG.getChild('Bargauge')

    def __init__(self, value, stylesheet, style, level):

        sty = stylesheet.styles.getstyle(style, 'bar', level)
        paraP.log.debug(style + "-BAR" + str(level) +
                        "(" + str(sty) + "): " + value)
        paraP.log.debug(str(sty.parent))
        self.style = sty
        self.v = Fraction(value)

    def wrap(self, availWidth, _availHeight):
        self.height = self.style.leading
        self.width = availWidth
        return (self.width, self.height)

    def draw(self):

        c = self.canv
        s = self.style

        if c._lineWidth != s.borderWidth:
            c.setLineWidth(s.borderWidth)
        if c._strokeColorObj != s.borderColor:
            c.setStrokeColor(s.borderColor)
        if c._strokeColorObj != s.borderColor:
            c.setStrokeColor(s.borderColor)
        if c._fillColorObj != s.textColor:
            c.setFillColor(s.textColor)

        lXY = (s.borderPadding, s.leading - s.fontSize)
        barW = self.width - (2 * s.borderPadding)
        fillW = s.borderPadding + (self.v * barW)
        if s.borderRadius:
            self.canv.roundRect(*lXY, barW, s.fontSize,
                                s.borderRadius, stroke=1, fill=0)
            self.canv.roundRect(*lXY, fillW, s.fontSize,
                                s.borderRadius, stroke=0, fill=1)
        else:
            self.canv.rect(*lXY, barW, s.fontSize, stroke=1, fill=0)
            self.canv.rect(*lXY, fillW, s.fontSize, stroke=0, fill=1)


class UnderlinedHeadingPararaph(Paragraph):

    def __init__(self, para):
        self.para = para

    def getSpaceBefore(self):
        return self.para.getSpaceBefore()

    def getSpaceAfter(self):
        return self.para.getSpaceAfter()

    def wrap(self, availWidth, availHeight):
        width, height = self.para.wrap(availWidth, availHeight)
        height += self.para.style.underlineOffset
        height += self.para.style.underlineWidth
        return (width, height)

    def draw(self):
        nlw = self.para.style.underlineWidth
        nsc = self.para.style.underlineColor
        olw = self.canv._lineWidth
        if nlw != olw:
            self.canv.setLineWidth(nlw)
        osc = self.canv._strokeColorObj
        if nsc != osc:
            self.canv.setStrokeColor(nsc)

        self.canv.line(0, -1 * self.para.style.underlineOffset,
                       self.para.width, 0)
        self.para.drawOn(self.canv, 0, 0)


class InlineHeadingParagraph(Flowable):

    def __init__(self, heading, para):
        self.heading = heading
        self.para = para

    def getSpaceBefore(self):
        return max(self.heading.getSpaceBefore(), self.para.getSpaceBefore())

    def getSpaceAfter(self):
        return max(self.heading.getSpaceAfter(), self.para.getSpaceAfter())

    def draw(self):
        headY = self.para.height - self.heading.height
        self.heading.drawOn(self.canv, 0, headY)
        self.para.drawOn(self.canv, 0, 0)

    def wrap(self, availWidth, availHeight):
        headingW, headingH = self.heading.wrap(availWidth, availHeight)

        self.para.style = self.para.style.clone('')

        self.para.style.firstLineIndent = self.heading.getActualLineWidths0()[
            0]
        paraW, paraH = self.para.wrap(availWidth, availHeight)
        self.width = max(paraW, headingW)
        self.height = max(paraH, headingH)
        return (self.width, self.height)


class iconBox(Flowable):

    def __init__(self, heads, tails, icon, iconWidth=30 * mm, iconHeight=15 * mm):
        self.iconWidth = iconWidth
        self.iconHeight = iconHeight
        icon = svg2rlg(icon)
        iW = icon.width
        iH = icon.height
        self.spaceBefore = 5

        iS = min(iconWidth / iW, iconHeight / iH)

        icon.scale(iS, iS)
        icon.height *= iS
        icon.width *= iS

        self.tdata = [[heads, icon], [tails, None]]
        self.tstyle = [('LEFTPADDING', (0, 0), (-1, -1), 0),
                       ('TOPPADDING', (0, 0), (-1, -1), 0),
                       ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                       ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                       ('ALIGNMENT', (-1, 0), (-1, 0), 'CENTER'),
                       ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                       ('VALIGN', (-1, 0), (-1, 0), 'MIDDLE'),
                       ('SPAN', (0, -1), (-1, -1))]

    def wrap(self, availWidth, availHeight):

        colWidths = [availWidth - self.iconWidth, self.iconWidth]
        self.table = Table(self.tdata, colWidths,
                           style=self.tstyle, spaceBefore=5)
        return self.table.wrap(availWidth, availHeight)

    def draw(self):
        self.table._drawOn(self.canv)


class paraH(Paragraph):
    log = LOG.getChild('paraH')

    def __init__(self, text, stylesheet, style, level, **kw):
        sty = stylesheet.styles.getstyle(style, 'h', level)
        paraH.log.debug("{}-H{!s}({}):{}".format(style, level, sty.name, text))
        super().__init__(text, sty, **kw)


class paraP(Paragraph):
    log = LOG.getChild('Paragraph')

    def __init__(self, text, stylesheet, style, level, **kw):

        sty = stylesheet.styles.getstyle(style, 'p', level)
        paraP.log.debug(style + "-P" + str(level) +
                        "(" + str(sty) + "): " + text)
        paraP.log.debug(str(sty.parent))
        super().__init__(text, sty, **kw)
