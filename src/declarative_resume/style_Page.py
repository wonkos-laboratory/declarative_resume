'''
Created on 11 Apr 2019

@author: wonko
'''
import logging

from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.platypus import doctemplate
from reportlab.platypus.doctemplate import PageTemplate
from reportlab.platypus.frames import Frame

import declarative_resume.style as style


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger('style_page')
LOG.setLevel(logging.DEBUG)


class PageLayout():

    log = LOG.getChild('PageLayout')
    log.setLevel(logging.DEBUG)

    def __init__(self, pagesize=A4, pagemargin=[0],
                 identity=None, header=None,
                 columns=None, _footer=None, _watermarks=None,
                 unit=mm, border=False, nextlayout='auto', background=None):

        self.nextlayout = nextlayout
        self.pagesize = pagesize
        self.id = identity
        self.unit = unit
        self.log.debug(pagemargin)
        pagemargin = style.conv(pagemargin, unit)
        self.log.debug(pagemargin)
        self.avail_space = style.geo(*self.pagesize, *pagemargin)
        self.log.debug(self.pagesize)
        self.log.debug(self.avail_space)
        self.border = border
        self.background = background
        self.set_header(header)

        if columns:
            self.set_columns(columns)

    def set_header(self, height, **kw):
        if not height:
            self._header = False
            return
        height = style.conv(height, self.unit)

        self.avail_space.y2 -= height
        self.avail_space.height -= height

        self._headerCoord = [self.avail_space.x1,
                             self.avail_space.y2, self.avail_space.width, height]

        if len(kw) == 0:
            kw = {'leftPadding': 0, 'bottomPadding': 0,
                  'rightPadding': 0, 'topPadding': 0}
        kw['id'] = 'header'
        kw['showBoundary'] = self.border
        self._headerConf = kw
        self._header = True

    def set_columns(self, columns=[], **kw):
        self._columns = []
        prevcol_x = self.avail_space.x1
        for i, column in enumerate(columns):
            if isinstance(column, str) and column.isnumeric():
                pad = float(column)
                pad = style.conv(pad, self.unit)
                prevcol_x += pad
                continue

            width = style.conv(column, self.unit)
            coord = [prevcol_x, self.avail_space.y1,
                     width, self.avail_space.height]
            kw = {'leftPadding': 0, 'bottomPadding': 0,
                  'rightPadding': 0, 'topPadding': 0}
            kw['id'] = 'col_' + str(i)
            kw['showBoundary'] = self.border
            self._columns.append([coord, kw])
            prevcol_x += width

    @property
    def pagesize(self):
        return self._pagesize

    @pagesize.setter
    def pagesize(self, size):
        self._pagesize = size
        if size:
            self.height = size[1]
            self.width = size[0]

    def paint_background(self, canv, _doc):
        pcol = self.background
        canv.setFillColor(pcol)
        canv.rect(-5, -5, self.pagesize[0] + 5,
                  self.pagesize[1] + 5, fill=1, stroke=0)

    def mkPageTemplate(self):
        frames = []
        if self._header:
            self.log.debug(self._headerCoord)
            frames.append(Frame(*self._headerCoord, **self._headerConf))
        for column in self._columns:
            self.log.debug(column[0], column[1])
            frames.append(Frame(*column[0], **column[1]))

        kw = {}
        if self.nextlayout == 'auto':
            kw['autoNextPageTemplate'] = True
            if self.background:
                on_page = self.paint_background
            else:
                on_page = doctemplate._doNothing
        return PageTemplate(id=self.id, frames=frames, onPage=on_page, **kw)
