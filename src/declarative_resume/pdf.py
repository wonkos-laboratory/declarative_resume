'''
Created on 1 March 2019

@author: wonko
'''

import logging
import string

from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus.doctemplate import BaseDocTemplate, FrameBreak
from reportlab.platypus.flowables import PageBreak

from declarative_resume.data import contentData, find_file
from declarative_resume.style_paragraph import paraP, paraH, listTable, bargauge,\
    InlineHeadingParagraphStyle, InlineHeadingParagraph,\
    UnderlinedParagraphStyle, UnderlinedHeadingPararaph, iconBox


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger('pdf')
LOG.setLevel(logging.DEBUG)


class Document():

    def __init__(self, contentFile, sections):
        self.data = contentData(contentFile)
        self.sections = sections

    def render(self, destinationFile, stylesheet, language):
        doc = ResumeTemplate(destinationFile, self.data, language,
                             stylesheet, allowSplitting=False)

        for section in self.sections:
            if section[0].lower() == 'framebreak':
                doc.frameBreak()
            elif section[0].lower() == 'pagebreak':
                doc.pageBreak()
            else:
                doc.add_paras(section[0], style=section[1])
        doc.build()


class ResumeTemplate(BaseDocTemplate):
    log = LOG.getChild('ResumeTemplate')

    def __init__(self, filename, data, lang, stylesheet, **kw):
        kw['pageTemplates'] = kw.get('pageTemplates', [])
        for page in stylesheet.pages.values():
            kw['pageTemplates'].append(page.mkPageTemplate())
        BaseDocTemplate.__init__(self, filename, **kw)
        self.lang = lang
        self.filename = filename
        self.stylesheet = stylesheet
        self.contentData = data
        self.flowables = []
        self.get_subs()

        self.flowables = []

        self.canvas = Canvas(filename)

    def get_subs(self):
        subs = self.contentData.getData('substitutions')
        self.subs = {}
        for k, v in subs.items():
            self.subs[k] = self.get_i18n(v)

    def get_i18n(self, data, default=None):
        log = LOG.getChild('i18n')
        log.debug('lang: ' + self.lang + ' data: ' + str(data))
        ts = None
        if isinstance(data, dict):
            if self.lang in data:
                log.debug('Lang is in data')
                ts = data[self.lang]
            elif default and default in data:
                ts = data[default]

        elif isinstance(data, str):
            ts = data

        if self.subs and ts:
            tem = string.Template(ts)
            ts = tem.substitute(self.subs)
        log.debug('ts' + str(ts))
        return ts

    def _paras_from_string(self, string_data, level, style, story=None):
        story.append(paraP(string_data, self.stylesheet, style, level))

    def _paras_from_list(self, list_data, level, style, story=None):
        for item in list_data:
            self.mk_paras(item, level=level, style=style, story=story)

    def _paras_from_dict(self, dict_data, level, style, story=None):
        heading = None
        icon = None
        text = []

        for key, value in dict_data.items():

            if key == "head":
                self.log.debug(value)
                heading = paraH(self.get_i18n(value),
                                self.stylesheet, style, level)

            elif key == "icon":
                icon = value

            elif key == "table":
                tdata = []
                if isinstance(value, dict):
                    rows = [value]
                else:
                    rows = value
                for row in rows:
                    tRow = []
                    self.mk_paras(row, style=style,
                                  level=level + 1, story=tRow)
                    tdata.append(tRow)
                text.append(listTable(tdata, self.stylesheet, style, level))

            elif key == "level":
                text.append(bargauge(value, self.stylesheet, style, level))

            elif key == 'list':
                ltext = ', '.join([self.get_i18n(val) for val in value])
                text.append(paraP(ltext, self.stylesheet, style, level))

            elif key == "text":
                textVals = value
                if not isinstance(textVals, list):
                    textVals = [textVals]
                for val in textVals:
                    ptext = self.get_i18n(val)
                    if ptext:
                        text.append(paraP(self.get_i18n(val),
                                          self.stylesheet, style, level))
                    else:
                        self.mk_paras(val, level=level + 1,
                                      style=style, story=text)

        if heading:
            if isinstance(heading.style, InlineHeadingParagraphStyle):
                heading = InlineHeadingParagraph(heading, text.pop(0))
            elif isinstance(heading.style, UnderlinedParagraphStyle):
                heading = UnderlinedHeadingPararaph(heading)

        if icon:

            heads = []
            tails = []

            icoFile = find_file(icon)

            if heading:
                heads.append(heading)

            for txt in text:
                if isinstance(txt, paraH):
                    heads.append(txt)
                else:
                    tails.append(txt)

            story.append(iconBox(heads, tails, icoFile))

        else:
            if heading:
                story.append(heading)

            for txt in text:
                story.append(txt)

    def mk_paras(self, data, level=0, style='base', story=None):
        if story is None:
            story = self.flowables

        pText = self.get_i18n(data)
        if pText:
            self._paras_from_string(pText, level, style, story)

        elif isinstance(data, list):
            self._paras_from_list(data, level, style, story)

        elif isinstance(data, dict):
            self._paras_from_dict(data, level, style, story)

    def add_paras(self, path=None, style='base'):
        if path:
            data = self.contentData.getData(path)
        else:
            data = self.contentData

        self.mk_paras(data, style=style)

    def frameBreak(self):
        self.flowables.append(FrameBreak())

    def pageBreak(self):
        self.flowables.append(PageBreak())

    def build(self):

        BaseDocTemplate.build(self, self.flowables, filename=self.filename)
