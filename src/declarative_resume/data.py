'''
Created on 1 March 2019

@author: wonko
'''

import json
import os


def find_file(name, path='.'):
    for root, _dirs, files in os.walk(path, followlinks=True):
        if name in files:
            return os.path.join(root, name)


class contentData(object):

    def __init__(self, filename):
        with open(filename, 'r') as json_file:
            self.data = json.load(json_file)

    def getData(self, path, dta=None):

        if not dta:
            dta = self.data

        if isinstance(path, str):
            path = path.split('.')

        for key in path:
            if key in dta:
                return dta[key]
            else:
                raise KeyError('{} not found in {}'.format(path, dta))
