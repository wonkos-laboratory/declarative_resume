'''
Created on 11 Apr 2019

@author: wonko
'''

import importlib
import logging

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import registerFontFamily
from reportlab.pdfbase.ttfonts import TTFont

from declarative_resume.style_Page import PageLayout
from declarative_resume.style_paragraph import ParagraphStyle


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger('style')
LOG.setLevel(logging.DEBUG)


class hierarchicalStyle(object):

    def __init__(self, style, parentstyle=None, rules=[]):
        self._style = style
        self.parent = parentstyle
        self.rules = rules
        self.rendered = False

    def getstyle(self):
        if not self.rendered and self.parent:
            for attr, rule in self.rules.items():
                value = rule(self._style, self.parent)
                setattr(self._style, attr, value)
            self.rendered = True
        return self._style


class colorscheme(object):

    def __init__(self, colors):
        self.colors = colors

    def hasColor(self, style):
        return style in self.colors

    def getColor(self, style, default='foreground'):
        return self.colors.get(style, self.colors[default])


class geo(object):
    log = LOG.getChild('Geo')
    log.setLevel(logging.DEBUG)

    def __init__(self, width, height, *arg):
        self.log.debug(arg)
        '''
        Make defining relative box things easy
        Mocks the behavior of the CSS avail_space/padding/etc Property

        Negative values are margins -> Within the given width/height
        Positive Values are Padding -> Outside the given width/heigh

        https://www.w3schools.com/cssref/pr_margin.asp

        If the avail_space property has four values:

            avail_space: 10px 5px 15px 20px;
                top avail_space is 10px
                right avail_space is 5px
                bottom avail_space is 15px
                left avail_space is 20px

        If the avail_space property has three values:

            avail_space: 10px 5px 15px;
                top avail_space is 10px
                right and left margins are 5px
                bottom avail_space is 15px

        If the avail_space property has two values:

            avail_space: 10px 5px;
                top and bottom margins are 10px
                right and left margins are 5px

        If the avail_space property has one value:

            avail_space: 10px;
                all four margins are 10px

        '''
        self._width = width
        self._height = height

        if len(arg) == 1:
            self.left = arg[0]
            self.right = arg[0]
            self.top = arg[0]
            self.bottom = arg[0]

        elif len(arg) == 2:
            self.left = arg[1]
            self.right = arg[1]
            self.top = arg[0]
            self.bottom = arg[0]

        elif len(arg) == 3:
            self.left = arg[1]
            self.right = arg[1]
            self.top = arg[0]
            self.bottom = arg[2]

        else:
            self.top = arg[0]
            self.right = arg[1]
            self.bottom = arg[2]
            self.left = arg[3]

        self.x1 = self.left * -1
        self.x2 = self._width + self.right
        self.y1 = self.bottom * -1
        self.y2 = self._height + self.top
        self.width = self.x2 - self.x1
        self.height = self.y2 - self.y1

    def __repr__(self):
        return str({'x1': self.x1, 'y1': self.y1, 'x1': self.x1, 'y2': self.y2, 'height': self.height, 'width': self.width})


def conv(values, unit):
    if isinstance(values, list):
        return [val * unit for val in values]
    else:
        return values * unit


class styletree(object):
    log = LOG.getChild('styletree')

    def __init__(self, styles={}, defaultStyle='base', stylesheet=None):
        self.styleheet = stylesheet
        self._styles = {}
        self.addstyles(styles)
        self.defaultStyle = defaultStyle

    def _calcstyles(self, maxl=None):
        for style, tags in self._styles.items():
            styletree.log.debug('calculating style: ' +
                                str(style) + 'tags:' + str(tags))
            for tag, levels in tags.items():
                if not maxl:
                    maxl = max(levels.keys())
                for l in range(1, maxl + 1):
                    if l not in self._styles[style][tag]:
                        parent = self._styles[style][tag][l - 1]
                        pstyle = parent.getstyle()
                        name = pstyle.name.rsplit(
                            '-', 1)[0] + "-" + tag + str(l)
                        self.addstyle(style, tag, l,
                                      ParagraphStyle(name, pstyle), pstyle, parent.rules)

    def getstylenode(self, path, allowDefault=True):

        style, tag, level = path

        if style not in self._styles and allowDefault:
            style = self.defaultStyle

        lev = self._styles[style].get(tag, self._styles[style].get('*'))

        if max(lev) < level:
            self._calcstyles(level)

        lev = self._styles[style].get(tag, self._styles[style].get('*'))

        styletree.log.debug('retrieving style node:' +
                            str(path) + '(' + str([style, tag, level]) + ')')
        return lev[level]

    def getstyle(self, style, tag, level):
        sty = self.getstylenode([style, tag, level])
        return sty.getstyle()

    def addstyles(self, styles):
        for sname, tags in styles.items():
            for tag, levels in tags.items():
                for level, sdef in levels.items():

                    stmod = sdef.get('stylemodule', 'reportlab.lib.styles')
                    stcl = sdef.get('styleclass', 'ParagraphStyle')
                    stkw = sdef.get('attr', {})
                    for key, value in stkw.items():
                        if 'color' in key.lower() and isinstance(value, str):
                            stkw[key] = self.styleheet.colors.getColor(value)

                    stkw['name'] = sname + '-' + tag + str(level)

                    styleclass = getattr(importlib.import_module(stmod), stcl)

                    if 'parent' in sdef and sdef['parent']:
                        pstyle = self.getstyle(*sdef['parent'])
                    else:
                        pstyle = None

                    stkw['parent'] = pstyle

                    style = styleclass(**stkw)
                    styletree.log.debug(sname + ' parameter: ' + str(stkw))
                    self.addstyle(sname, tag, level, style,
                                  pstyle, sdef.get('rules', {}))
        self._calcstyles()

    def addstyle(self, sname, tag, level, style, parentstyle=None, rules={}):
        styletree.log.debug('Adding Style: ' + str([sname, tag, level]) + ' ' + str(
            style) + ' parent' + str(parentstyle) + ' rules' + str(rules))
        if sname not in self._styles:
            self._styles[sname] = {}
        if tag not in self._styles[sname]:
            self._styles[sname][tag] = {}

        self._styles[sname][tag][level] = hierarchicalStyle(
            style, parentstyle, rules)


class stylesheet():

    def __init__(self, colors=None, styles=None, fonts=None, pages=None):
        self.colors = colors
        self.fonts = fonts
        self.styles = styles
        self._pages = {}
        if pages:
            self.pages = pages

    @property
    def styles(self):
        return self._styles

    @styles.setter
    def styles(self, styles):
        if styles:
            self._styles = styletree(styles, stylesheet=self)

    @property
    def colors(self):
        return self._colors

    @colors.setter
    def colors(self, colors):
        self._colors = colorscheme(colors)

    @property
    def pages(self):
        return self._pages

    @pages.setter
    def pages(self, pages_def):
        for page_name, page_def in pages_def.items():
            if self.colors.hasColor('page'):
                page_def['background'] = self.colors.getColor('page')
            self._pages[page_name] = PageLayout(**page_def)

    @property
    def fonts(self):
        return None

    @fonts.setter
    def fonts(self, fonts_def):
        if 'ttfonts' in fonts_def:
            for ttfont, file in fonts_def['ttfonts'].items():
                pdfmetrics.registerFont(TTFont(ttfont, file))
        if 'families' in fonts_def:
            for family, family_def in fonts_def['families'].items():
                registerFontFamily(family, **family_def)
