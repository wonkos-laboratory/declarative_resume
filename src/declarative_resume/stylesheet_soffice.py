'''
Created on 11 Apr 2019

@author: wonko
'''
from reportlab.lib.colors import HexColor, black, white, lightgrey
from reportlab.lib.enums import TA_RIGHT

from declarative_resume.style import stylesheet


# register with the postscript name
# ft1dump /usr/share/fonts/truetype/RobotoCondensed-Regular.ttf|grep PostScript
fonts = {
    'ttfonts': {'RobotoCondensed': 'RobotoCondensed-Regular.ttf',
                'RobotoCondensed-Light': 'RobotoCondensed-Light.ttf',
                'RobotoCondensed-Bold': 'RobotoCondensed-Bold.ttf',
                'RobotoCondensed-LightItalic': 'RobotoCondensed-LightItalic.ttf',
                'RobotoCondensed-BoldItalic': 'RobotoCondensed-BoldItalic.ttf',
                'Symbola': 'Symbola.ttf'
                },
    'families': {'RobotoCondensed-Light': {
        'normal': 'RobotoCondensed-Light',
        'bold': 'RobotoCondensed-Bold',
        'italic': 'RobotoCondensed-LightItalic',
        'boldItalic': 'RobotoCondensed-BoldItalic'}
    }

}


colors_soffice = {
    "yellow": HexColor('#ff9900'),
    "green": HexColor('#009900'),
    "black": black,
    "white": white,
    'gray': lightgrey
}


colors = {
    "foreground": colors_soffice['black'],
    "background": colors_soffice['white'],
    "level1": colors_soffice['green'],
    "level2": colors_soffice['yellow'],
    "lines": colors_soffice['gray']
}


styles = {
    'base': {
        '*': {
            0: {'styleclass': 'ParagraphStyle',
                'attr': {'fontName': 'RobotoCondensed',
                         'textColor': 'foreground',
                         'fontSize': 12,
                         'leading': 14},
                'parent': None,
                'rules': {"leftIndent": lambda _style, parent: parent.leftIndent + 15}
                }
        }
    },
    'title': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
                'attr': {
                    'alignment': TA_RIGHT,
                    'textColor': 'level2',
                    'fontName': 'RobotoCondensed-Bold',
                    'fontSize': 18,
                    'leading': 20
                }
            }
        },
        'h': {
            0: {
                'parent': ['title', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'fontSize': 32,
                    'leading': 34
                }
            }
        }
    },
    'abstract': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
                'attr': {
                    'firstLineIndent': -10,
                    'leftIndent': 10,
                    'spaceAfter': 6
                }
            }
        }},
    'life': {
        '*': {
            0: {'parent': ['base', '*', 0],
                'attr': {

                "leftIndent": 10}}
        },
        'h': {
            0: {
                'styleclass': 'InlineHeadingParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'parent': ['base', '*', 0],
                'attr': {
                    'bulletFontName': "Symbola",

                    'bulletIndent': -15,
                    'bulletAnchor': 'start',
                    'bulletText': "🌑",

                    'bulletColor': 'level1',

                }
            },
            1: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'firstLineIndent': 0,
                    'leftIndent': 5,
                    'spaceBefore': 5
                }
            },
            2: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level2',
                    'firstLineIndent': 0,
                    'leftIndent': 10
                }
            }
        }
    },
    'sections': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },

            1: {'parent': ['sections', '*', 0],
                'attr': {
                'leftIndent': 0,
                'firstLineIndent': 0,
                'textColor': black,
                'splitLongWords': False}
                }
        },
        'bar': {
            0: {'parent': ['sections', 'h', 0],
                'styleclass': 'BargaugeStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                'borderColor': 'level1',
                'borderPadding': 2,
                'borderRadius': 2,
                'textColor': 'level1'
            }
            },
        },
        'tab': {
            0: {'styleclass': 'ListTableStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {'cmds': [('LEFTPADDING', (0, 0), (0, -1), 0),
                                  ('RIGHTPADDING', (-1, 0), (-1, -1), 0),
                                  ('ALIGN', (0, 0), (-1, -1), 'RIGHT')]
                         }
                }
        }
    },
    'projects': {
        '*': {
            0: {
                'parent': ['base', '*', 0]
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },
            1: {
                'parent': ['base', '*', 0],
                'attr': {
                    'leftIndent': 0,
                    'firstLineIndent': 0,
                    'textColor': 'level2',
                    'splitLongWords': False,
                    'fontSize': 10,
                    'leading': 12,
                    'spaceBefore': 5
                }
            },
            2: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'fontName': 'RobotoCondensed-Bold',

                }

            },
            3: {
                'parent': ['projects', 'h', 1],
                'attr': {
                    'spaceBefore': 0, }

            }
        }
    },
    'lists': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },

            1: {'parent': ['sections', '*', 0],
                'attr': {
                'leftIndent': 0,
                'firstLineIndent': 0,
                'textColor': 'level2',
                'splitLongWords': False}
                }
        },
    }
}


pages = {
    'first': {'pagemargin': [-10, -10, -5, -20],
              'header': 30,
              'columns': [110, '10', 60]
              },
    'next': {
        'pagemargin': [-10, -10, -5, -20],
        'columns': [85, '10', 85]
    }

}


soffice = stylesheet(colors, styles, fonts, pages)
