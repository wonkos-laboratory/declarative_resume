'''
Created on 11 Apr 2019

@author: wonko
'''
from reportlab.lib.colors import HexColor
from reportlab.lib.enums import TA_RIGHT

from declarative_resume.style import stylesheet


colors_solarized = {
    "base03": HexColor('#002b36'),
    "base02": HexColor('#073642'),
    "base01": HexColor('#586e75'),
    "base00": HexColor('#657b83'),
    "base0": HexColor('#839496'),
    "base1": HexColor('#93a1a1'),
    "base2": HexColor('#eee8d5'),
    "base3": HexColor('#fdf6e3'),
    "yellow": HexColor('#b58900'),
    "orange": HexColor('#cb4b16'),
    "red": HexColor('#dc322f'),
    "magenta": HexColor('#d33682'),
    "violet": HexColor('#6c71c4'),
    "blue": HexColor('#268bd2'),
    "cyan": HexColor('#2aa198'),
    "green": HexColor('#859900')
}


colors = {
    "foreground": colors_solarized['base0'],
    "background": colors_solarized['base02'],
    "page": colors_solarized['base03'],
    "level1": colors_solarized['orange'],
    "level2": colors_solarized['yellow'],
    'lines': colors_solarized['orange']

}


pages = {
    'first': {'pagemargin': [-10, -10, -5, -20],
              'header': 30,
              'columns': [110, '10', 60]},
    'next': {'pagemargin': [-10, -10, -5, -20],
             'columns': [85, '10', 85]}
}


styles = {
    'base': {
        '*': {
            0: {'styleclass': 'ParagraphStyle',
                'attr': {'fontName': 'RobotoCondensed',
                         'textColor': 'foreground',
                         'fontSize': 12,
                         'leading': 14},
                'parent': None,
                'rules': {"leftIndent": lambda _style, parent: parent.leftIndent + 15}
                }
        }
    },
    'title': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
                'attr': {
                    'alignment': TA_RIGHT,
                    'textColor': 'level2',
                    'fontName': 'RobotoCondensed-Bold',
                    'fontSize': 18,
                    'leading': 20
                }
            }
        },
        'h': {
            0: {
                'parent': ['title', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'fontSize': 32,
                    'leading': 34
                }
            }
        }
    },
    'abstract': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
                'attr': {
                    'firstLineIndent': -10,
                    'leftIndent': 10,
                    'spaceAfter': 6
                }
            }
        }},
    'life': {
        '*': {
            0: {'parent': ['base', '*', 0],
                'attr': {

                "leftIndent": 10}}
        },
        'h': {
            0: {
                'styleclass': 'InlineHeadingParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'parent': ['base', '*', 0],
                'attr': {
                    'bulletFontName': "Symbola",

                    'bulletIndent': -15,
                    'bulletAnchor': 'start',
                    'bulletText': "🌑",

                    'bulletColor': 'level1',

                }
            },
            1: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'firstLineIndent': 0,
                    'leftIndent': 5,
                    'spaceBefore': 5
                }
            },
            2: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level2',
                    'firstLineIndent': 0,
                    'leftIndent': 10
                }
            }
        }
    },
    'sections': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },

            1: {'parent': ['sections', '*', 0],
                'attr': {
                'leftIndent': 0,
                'firstLineIndent': 0,
                'textColor': 'level2',
                'splitLongWords': False}
                }
        },
        'bar': {
            0: {'parent': ['sections', 'h', 0],
                'styleclass': 'BargaugeStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                'borderColor': 'foreground',
                'borderPadding': 2,
                'borderRadius': 2,
                'textColor': 'foreground'
            }
            },
        },
        'tab': {
            0: {'styleclass': 'ListTableStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {'cmds': [('LEFTPADDING', (0, 0), (0, -1), 0),
                                  ('RIGHTPADDING', (-1, 0), (-1, -1), 0),
                                  ('ALIGN', (0, 0), (-1, -1), 'RIGHT')]
                         }
                }
        }
    },
    'projects': {
        '*': {
            0: {
                'parent': ['base', '*', 0]
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },
            1: {
                'parent': ['base', '*', 0],
                'attr': {
                    'leftIndent': 0,
                    'firstLineIndent': 0,
                    'textColor': 'level2',
                    'splitLongWords': False,
                    'fontSize': 10,
                    'leading': 12,
                    'spaceBefore': 5
                }
            },
            2: {
                'parent': ['base', '*', 0],
                'attr': {
                    'textColor': 'level1',
                    'fontName': 'RobotoCondensed-Bold',

                }

            },
            3: {
                'parent': ['projects', 'h', 1],
                'attr': {
                    'spaceBefore': 0, }

            }
        }
    },
    'lists': {
        '*': {
            0: {
                'parent': ['base', '*', 0],
            }
        },
        'h': {
            0: {
                'parent': ['sections', '*', 0],
                'styleclass': 'UnderlinedParagraphStyle',
                'stylemodule': 'declarative_resume.style_paragraph',
                'attr': {
                    'fontName': 'RobotoCondensed-Bold',
                    'textColor': 'level1',
                    'underlineColor': 'lines',
                    'underlineWidth': 1.5,
                    'underlineOffset': 2.2,
                    'spaceAfter': 5,

                }
            },

            1: {'parent': ['sections', '*', 0],
                'attr': {
                'leftIndent': 0,
                'firstLineIndent': 0,
                'textColor': 'level2',
                'splitLongWords': False}
                }
        },
    }
}

fonts = {
    'ttfonts': {'RobotoCondensed': 'RobotoCondensed-Regular.ttf',
                'RobotoCondensed-Light': 'RobotoCondensed-Light.ttf',
                'RobotoCondensed-Bold': 'RobotoCondensed-Bold.ttf',
                'RobotoCondensed-LightItalic': 'RobotoCondensed-LightItalic.ttf',
                'RobotoCondensed-BoldItalic': 'RobotoCondensed-BoldItalic.ttf',
                'Symbola': 'Symbola.ttf'
                },
    'families': {'RobotoCondensed-Light': {
        'normal': 'RobotoCondensed-Light',
        'bold': 'RobotoCondensed-Bold',
        'italic': 'RobotoCondensed-LightItalic',
        'boldItalic': 'RobotoCondensed-BoldItalic'}
    }

}

solarized = stylesheet(colors, styles, fonts, pages)
