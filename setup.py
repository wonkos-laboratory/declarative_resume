'''

@author: wonko
'''

from setuptools import setup

setup(name='declarative_resume',
      version='0.0.0',
      description='Generate Resume Documents from Data not Documents',
      author='wonko',
      author_email='42@wonko.de',
      packages=['declarative_resume'],
      package_dir={'': 'src'},
      license='GPLv3',
      install_requires=['reportlab',
                        'svglib',
                        'Pillow'],
      test_suite="test"
      )
